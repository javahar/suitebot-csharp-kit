﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using suitebot_Csharp_kit.BotAI;
using suitebot_Csharp_kit.Game;
using suitebot_Csharp_kit.Server;


namespace suitebot_Csharp_kit.src
{
    public class BrokenBot : IBotAi
    {
        private static readonly List<Move> DoubleMoves = new List<Move>()
        {
            Move.UpUp,
            Move.UpDown,
            Move.UpLeft,
            Move.UpRight,
            Move.DownUp,
            Move.DownDown,
            Move.DownLeft,
            Move.DownRight,
            Move.LeftUp,
            Move.LeftDown,
            Move.LeftLeft,
            Move.LeftRight,
            Move.RightUp,
            Move.RightDown,
            Move.RightLeft,
            Move.RightRight
        };
        public string GetName()
        {
            return "BrokenBot";
        }

        public Move MakeMove(IGameState gameState, int botId)
        {
            var rand = new Random();
            return DoubleMoves[rand.Next(0, 16)];
        }
    }

    public class Jbot : IBotAi
    {
        private static readonly List<Move> SingleMoves = new List<Move>()
        {
            Move.Up,
            Move.Down,
            Move.Left,
            Move.Right
        };

        private static readonly List<Move> DoubleMoves = new List<Move>()
        {
            Move.UpUp,
            Move.UpDown,
            Move.UpLeft,
            Move.UpRight,
            Move.DownDown,
            Move.DownLeft,
            Move.DownRight,
            Move.LeftUp,
            Move.LeftDown,
            Move.LeftLeft,
            Move.RightUp,
            Move.RightDown,
            Move.RightRight
        };

        private int _botId;
        private IGameState _gameState;
        Point currentLoc;

        public Point GetLocation()
        {
            return currentLoc;
        }



        public Jbot()
        {
           
        }

       
        public string GetName()
        {
            return "JbotAi";
        }

        public Move MakeMove(IGameState gameState, int botId)
        {
            _botId = botId;
            _gameState = gameState;
            int x=0, y=0;
            setCurrentLocation( gameState.GetBotLocation(botId));
            calculateNextStep();
            return doStep(x,y);

         }

        private Point calculateNextStep()
        {
            var map = generateMap();
          
            return new Point(0, 0);
        }

        public int[,] generateMap()
        {
            var map = new int[_gameState.GetPlanWidth(), _gameState.GetPlanHeight()];
            foreach (var obst in _gameState.GetObstacleLocations())
                map[obst.X, obst.Y] = 100;

            foreach (var bot in _gameState.GetAllBotIds())
            {
                var head = _gameState.GetBotLocation(bot);
                map[head.X, head.Y] = 100;
            }

            return map;
        }

        public void setCurrentLocation(Point point)
        {
            currentLoc = point;
        }

        public Move doStep( int x, int y)
        {
            return Move.Down;
        }

    }
}
