﻿using System;
using System.Collections.Generic;
using suitebot_Csharp_kit.Game;

namespace suitebot_Csharp_kit.BotAI
{
	public class SampleBotAi : IBotAi
	{
		private static readonly List<Move> SingleMoves = new List<Move>()
		{
			Move.Up,
			Move.Down,
			Move.Left,
			Move.Right
		};

		private static readonly List<Move> DoubleMoves = new List<Move> ()
		{
			Move.UpUp,
			Move.UpDown,
			Move.UpLeft,
			Move.UpRight,
			Move.DownUp,
			Move.DownDown,
			Move.DownLeft,
			Move.DownRight,
			Move.LeftUp,
			Move.LeftDown,
			Move.LeftLeft,
			Move.LeftRight,
			Move.RightUp,
			Move.RightDown,
			Move.RightLeft,
			Move.RightRight
		};

		private int _botId;
		private IGameState _gameState;

		private readonly Predicate<Move> isMoveToTreasure;
		private readonly Predicate<Move> isMoveToBattery;
		private readonly Predicate<Move> isSafeMove;
//
//	private final Supplier<Stream<Move>> closeTreasureMoveSupplier = () -> SINGLE_MOVES.stream().filter(isMoveToTreasure);
//	private final Supplier<Stream<Move>> closeBatteryMoveSupplier = () -> SINGLE_MOVES.stream().filter(isMoveToBattery);
//	private final Supplier<Stream<Move>> reachableTreasureMoveSupplier = () -> DOUBLE_MOVES.stream().filter(isMoveToTreasure);
//	private final Supplier<Stream<Move>> reachableBatteryMoveSupplier = () -> DOUBLE_MOVES.stream().filter(isMoveToBattery);
//	private final Supplier<Stream<Move>> safeMoveSupplier = () -> SINGLE_MOVES.stream().filter(isSafeMove);


		public SampleBotAi()
		{
			isMoveToTreasure = move => _gameState.GetTreasureLocations().Contains(Destination(move));
			isMoveToBattery = move => _gameState.GetBatteryLocations().Contains(Destination(move));
			isSafeMove = move => !_gameState.GetObstacleLocations().Contains(Destination(move));
		}


		/**
		 * If a treasure is close (distance 1), go to it;
		 * otherwise, if a battery is close, go to it;
		 * otherwise, if a treasure is reachable (distance 2), go to it;
		 * otherwise, if a battery is reachable, go to it;
		 * otherwise, if a safe move can be made (one that avoids any obstacles), do it;
		 * otherwise, go down.
		 */
		public Move MakeMove(IGameState gameState, int botId)
		{
			_botId = botId;
			_gameState = gameState;


			if (IsDead())
				return null;

		    var treasureMove = SingleMoves.Find(move => isMoveToTreasure(move));
		    if (treasureMove != null)
		        return treasureMove;

		    var batteryMove = SingleMoves.Find(move => isMoveToBattery(move));
		    if (batteryMove != null)
		        return batteryMove;
		        		        

/*		return Stream.of(
				closeTreasureMoveSupplier,
				closeBatteryMoveSupplier,
				reachableTreasureMoveSupplier,
				reachableBatteryMoveSupplier,
				safeMoveSupplier
		)
				.map(Supplier::get)
				.map(System.IO.Stream::findFirst)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.findFirst()
				.orElse(Move.DOWN_);*/
			return Move.Down;
		}

		private bool IsDead()
		{
			return !_gameState.GetLiveBotIds().Contains(_botId);
		}

		private Point Destination(Move move)
		{
			var botLocation = _gameState.GetBotLocation(_botId);
			var destination = Direction.FromPosition(botLocation, move.Step1);

			return move.Step2?.FromPosition(destination) ?? destination;
		}


		public string GetName()
		{
			return "Sample AI";
		}
	}
}