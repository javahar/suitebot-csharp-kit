﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;

namespace suitebot_Csharp_kit.Server
{
    public class SimpleServer
    {
        public const string ShutdownRequest = "EXIT";
        public const string UptimeRequest = "UPTIME";

        private readonly int _port;
        private readonly ISimpleRequestHandler _requestHandler;

        private bool _shouldShutDown;
        private Stopwatch _stopwatch;

        public SimpleServer(int port, ISimpleRequestHandler requestHandler)
        {
            _port = port;
            _requestHandler = requestHandler;
        }

        public void Run()
        {
            RunInternal();
        }

        private void RunInternal()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();

            var client = new TcpListener(IPAddress.Loopback, _port);

		try {	            
	            client.Start(20);


	            while (!_shouldShutDown)
	            {
					var handler = client.AcceptSocket();
					handler.ReceiveTimeout = 0;
					handler.SendTimeout = 0;
					HandleRequest(handler);
					handler.Close();
	            }
			}
			finally {
				client.Stop();
			}
        }

        private void HandleRequest(Socket socket)
        {
            var bytes = new byte[4096];
            var bytesAccepted = socket.Receive(bytes);

			var request = Encoding.ASCII.GetString(bytes, 0, bytesAccepted).Trim();
            string response;
            switch (request)
            {
                case "":
                    return;
                case ShutdownRequest:
                    _shouldShutDown = true;
                    return;
                case UptimeRequest:
                    response = _stopwatch.Elapsed.Seconds.ToString(); 
                    break;
                default:
                    response = _requestHandler.ProcessRequest(request);
                    break;
            }

            socket.Send(Encoding.ASCII.GetBytes(response));
        }
    }
}
