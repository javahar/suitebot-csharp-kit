﻿namespace suitebot_Csharp_kit.Game
{
	public class Move
	{
		public static readonly Move Up = new Move(Direction.Up);
		public static readonly Move Down = new Move(Direction.Down);
		public static readonly Move Left = new Move(Direction.Left);
		public static readonly Move Right = new Move(Direction.Right);

		public static readonly Move UpUp = new Move(Direction.Up,  Direction.Up);
		public static readonly Move UpDown = new Move(Direction.Up, Direction.Down);
		public static readonly Move UpLeft = new Move(Direction.Up, Direction.Left);
		public static readonly Move UpRight = new Move(Direction.Up, Direction.Right);

		public static readonly Move DownUp = new Move(Direction.Down, Direction.Up);
		public static readonly Move DownDown = new Move(Direction.Down, Direction.Down);
		public static readonly Move DownLeft = new Move(Direction.Down, Direction.Left);
		public static readonly Move DownRight = new Move(Direction.Down, Direction.Right);

		public static readonly Move LeftUp = new Move(Direction.Left, Direction.Up);
		public static readonly Move LeftDown = new Move(Direction.Left, Direction.Down);
		public static readonly Move LeftLeft = new Move(Direction.Left, Direction.Left);
		public static readonly Move LeftRight = new Move(Direction.Left, Direction.Right);

		public static readonly Move RightUp = new Move(Direction.Right, Direction.Up);
		public static readonly Move RightDown = new Move(Direction.Right, Direction.Down);
		public static readonly Move RightLeft = new Move(Direction.Right, Direction.Left);
		public static readonly Move RightRight = new Move(Direction.Right, Direction.Right);

		public readonly Direction Step1;
		public readonly Direction Step2;

		public Move(Direction step1) : this(step1, null)
		{
		}

		public Move(Direction step1, Direction step2)
		{
			Step1 = step1;
			Step2 = step2;
		}
			
		public override bool Equals(object o)
		{
			if (this == o) return true;

			var move = o as Move;
			if (move == null) return false;

			return Step1 == move.Step1 && Step2 == move.Step2;
		}
			
		public override int GetHashCode()
		{
			var result = Step1.GetHashCode();
			result = 31 * result + (Step2 != null ? Step2.GetHashCode() : 0);
			return result;
		}
			
		public override string ToString()
		{
		    return Step1 + (Step2?.ToString() != null ? Step2?.ToString() : "");
		}
	}

}

