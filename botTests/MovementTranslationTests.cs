﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using suitebot_Csharp_kit.Game;

namespace botTests
{
    [TestClass]
    public class MovementTranslationTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var bot = new suitebot_Csharp_kit.src.Jbot();

            Assert.AreEqual(Move.Down, bot.doStep(1, 1));

        }
    }
}
