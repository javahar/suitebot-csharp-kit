﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using suitebot_Csharp_kit.BotAI;
using suitebot_Csharp_kit.Game;
using suitebot_Csharp_kit.Server;
using System.Collections.Generic;
using suitebot_Csharp_kit.src;

namespace botTests
{
    [TestClass]
    public class stepTest
    {
        [TestMethod]
        public void BotLocation()
        {
            var input = new List<string>();

            input.Add("  **");
            input.Add("  1*");
            input.Add("  **");

            var gameState= GameStateFactory.CreateFromString(input);
            Assert.AreEqual(4, gameState.GetPlanWidth());
            Assert.AreEqual(3, gameState.GetPlanHeight());

            var bot = new Jbot();
            bot.MakeMove(gameState,1);
            Assert.AreEqual(2, bot.GetLocation().X);
            Assert.AreEqual(1, bot.GetLocation().Y);



        }
        [TestMethod]
        public void MapObsticles()
        {
            var input = new List<string>();

            input.Add("  **");
            input.Add("  1*");
            input.Add("  **");

            var gameState = GameStateFactory.CreateFromString(input);
            Assert.AreEqual(4, gameState.GetPlanWidth());
            Assert.AreEqual(3, gameState.GetPlanHeight());

            var bot = new Jbot();
            bot.MakeMove(gameState, 1);
            var map= bot.generateMap();

            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(0, map[ 0,i]);
                Assert.AreEqual(0, map[ 1, i]);
                Assert.AreEqual(100, map[ 2, i]);
                Assert.AreEqual(100, map[ 3, i]);
            }
        }
    }
}
