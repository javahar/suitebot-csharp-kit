﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using suitebot_Csharp_kit.Game;

namespace suitebot_Csharp_kitTests.Game
{

    [TestClass]
    public class UnitTest1
    {
        private readonly Point _from = new Point(3, 7);

        [TestMethod]
        public void DirectionUpTest()
        {
            Assert.AreEqual(Direction.Up.FromPosition(_from), new Point(3, 6));
        }

        [TestMethod]
        public void DirectionLeftTest()
        {
            Assert.AreEqual(Direction.Left.FromPosition(_from), new Point(2, 7));
	    }

        [TestMethod]
        public void DirectionRightTest()
        {
            Assert.AreEqual(Direction.Right.FromPosition(_from), new Point(4, 7));
        }

        [TestMethod]
        public void DirectionDownTest()
        {
            Assert.AreEqual(Direction.Down.FromPosition(_from), new Point(3, 8));
        }
    }
}
